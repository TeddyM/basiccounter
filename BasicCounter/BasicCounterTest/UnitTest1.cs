﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterLib;

namespace BasicCounterTest
{
    [TestClass]
    public class BasicCounterTest
    {

        [TestMethod]
        public void TestIncrementationCompteur()
        {

            BasicCounterClass Counter = new BasicCounterClass(2);
            Counter.IncrementationCompteur();
            Assert.AreEqual(3, Counter.getCounter());
        }

        [TestMethod]
        public void TestRAZCompteur()
        {
            BasicCounterClass Counter = new BasicCounterClass(20);
            Counter.RAZCompteur();
            Assert.AreEqual(0, Counter.getCounter());
        }

        [TestMethod]
        public void TestDecrementatonCompteur()
        {
            BasicCounterClass Counter = new BasicCounterClass(18);
            Counter.DecrementationCompteur();
            Assert.AreEqual(17, Counter.getCounter());
        }

        [TestMethod]
        public void TestGetCounter()
        {
            BasicCounterClass Counter = new BasicCounterClass(10);
            Assert.AreEqual(10, Counter.getCounter());
        }
    }
}