﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterLib
{
    public class BasicCounterClass
    {
        private int compteur;

        public BasicCounterClass(int compteur)
        {
            this.compteur = compteur;
        }
   
        public void IncrementationCompteur()
        {
            compteur = compteur + 1;
        }

        public void DecrementationCompteur()
        {
            compteur = compteur - 1;
        }

        public void RAZCompteur()
        {
            compteur = 0;
        }

        public int getCounter()
        {
            return compteur;
        }
    }
}
